<?php

function simple_theme_enqueue()
{
    wp_enqueue_style('simplestyle', get_template_directory_uri() . '/css/simple.css', [], '1.0.0', 'all');
    wp_enqueue_script('simplejs', get_template_directory_uri() . '/js/simple.js', [], '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'simple_theme_enqueue');

function simple_theme_setup()
{
    add_theme_support('menus');

    register_nav_menu('header', 'Header Navigation');
    register_nav_menu('footer', 'Footer Navigation');
}

add_action('init', 'simple_theme_setup');